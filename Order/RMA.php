<?php

/**
 * @file
 *          Magento API Client (SOAP v1).
 *          Allows wrappers for each call, dependencies injections
 *          and code completion.
 *
 * @author  Sébastien MALOT <sebastien@malot.fr>
 * @license MIT
 * @url     <https://github.com/smalot/magento-client>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Smalot\Magento\Order;

use Smalot\Magento\ActionInterface;
use Smalot\Magento\MagentoModuleAbstract;

/**
 * Class RMA
 *
 * @package Smalot\Magento\Order
 */
class RMA extends MagentoModuleAbstract
{
    /**
     * Allows you to retrieve the required rma information.
     *
     * @param string $orderIncrementId
     *
     * @return ActionInterface
     */
    public function getInfo()
    {
        return $this->__createAction('oro_rma.list', func_get_args());
    }   
}
